package ru.t1c.babak.tm.exception.system;

import ru.t1c.babak.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
