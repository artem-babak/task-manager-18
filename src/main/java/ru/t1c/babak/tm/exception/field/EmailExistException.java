package ru.t1c.babak.tm.exception.field;

public final class EmailExistException extends AbstractFieldException {

    public EmailExistException() {
        super("Error! Email already exist...");
    }

}
