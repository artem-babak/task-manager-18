package ru.t1c.babak.tm.exception.entity;

public final class UserNotCreatedException extends AbstractEntityNotFoundException {

    public UserNotCreatedException() {
        super("Error! User not created...");
    }

}
