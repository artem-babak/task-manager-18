package ru.t1c.babak.tm.repository;

import ru.t1c.babak.tm.api.repository.IUserRepository;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User create(String login) {
        final User user = new User();
        user.setLogin(login);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash, String email) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash, Role role) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        return add(user);
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public int getSize() {
        return users.size();
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User setPasswordHash(final User user, final String passwordHash) {
        if (user == null) return null;
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Override
    public User setPasswordHashById(final String id, final String passwordHash) {
        final User user = findById(id);
        if (user == null) return null;
        setPasswordHash(user, passwordHash);
        return user;
    }

}
