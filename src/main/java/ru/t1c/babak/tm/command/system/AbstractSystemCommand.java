package ru.t1c.babak.tm.command.system;

import ru.t1c.babak.tm.api.service.ICommandService;
import ru.t1c.babak.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
