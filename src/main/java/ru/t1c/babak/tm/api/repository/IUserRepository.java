package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User add(User user);

    User create(String login);

    User create(String login, String passwordHash);

    User create(String login, String passwordHash, String email);

    User create(String login, String passwordHash, Role role);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

    int getSize();

    void clear();

    User setPasswordHash(User user, String passwordHash);

    User setPasswordHashById(String id, String passwordHash);

}
