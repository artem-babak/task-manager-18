package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

//    Boolean isLoginExists(String login);

    User findByEmail(String email);

//    Boolean isEmailExists(String email);

    User add(User project);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String id, String password);

    User updateById(String id, String firstName, String lastName, String middleName);

    User updateByLogin(String id, String firstName, String lastName, String middleName);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByEmail(String email);

//    void remove(User user);
//
//    void removeById(String id);
//
//    void removeByLogin(String login);
//
//    void clear();

}
