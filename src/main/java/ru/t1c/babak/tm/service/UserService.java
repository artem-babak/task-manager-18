package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IUserRepository;
import ru.t1c.babak.tm.api.service.IUserService;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.exception.entity.UserNotCreatedException;
import ru.t1c.babak.tm.exception.entity.UserNotFoundException;
import ru.t1c.babak.tm.exception.field.*;
import ru.t1c.babak.tm.model.User;
import ru.t1c.babak.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User updateById(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User updateByLogin(final String login, final String firstName, final String lastName, final String middleName) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (userRepository.findByLogin(login) == null) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = userRepository.create(login, passwordHash);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (userRepository.findByLogin(login) != null) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        if (userRepository.findByEmail(email) != null) throw new EmailExistException();
        User user = userRepository.create(login, passwordHash, email);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (userRepository.findByLogin(login) != null) throw new LoginExistException();
        if (role == null) throw new RoleEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = userRepository.create(login, passwordHash, role);
        if (user == null) throw new UserNotCreatedException();
        return user;
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return remove(findById(id));
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return remove(findByLogin(login));
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return remove(findByEmail(email));
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final String passwordHash = HashUtil.salt(password);
        User user = userRepository.setPasswordHashById(id, passwordHash);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
